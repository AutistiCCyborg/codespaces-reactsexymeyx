const express = require('express');
const app = express();
const fs = require('fs');

app.use(express.json());

app.post('/generateKey', (req, res) => {
  const { authCode } = req.body;
  const shortKey = authCode.substring(0, 8);

  fs.readFile('./keys.json', 'utf-8', (err, data) => {
    if (err) {
      console.error(err);
      return res.status(500).send('Error reading keys file');
    }

    let keys;
    try {
      keys = JSON.parse(data);
    } catch (error) {
      console.error(error);
      return res.status(500).send('Error parsing keys file');
    }

    keys[shortKey] = authCode;

    fs.writeFile('./keys.json', JSON.stringify(keys), 'utf-8', (err) => {
      if (err) {
        console.error(err);
        return res.status(500).send('Error writing to keys file');
      }

      return res.send(shortKey);
    });
  });
});

app.listen(3000, () => {
  console.log('Server listening on port 3000');
});
