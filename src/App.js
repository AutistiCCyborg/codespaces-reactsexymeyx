import React from 'react';
import './App.css';
import { DynamicContextProvider, DynamicWidget } from '@dynamic-labs/sdk-react';
import { useDynamicContext } from '@dynamic-labs/sdk-react';
import axios from 'axios';
import './bootstrap.min.css';


const SignMessageButton = () => {
  const { primaryWallet } = useDynamicContext();

  const [shortUniqueKey, setShortUniqueKey] = React.useState('');
  const [feedbackMessage, setFeedbackMessage] = React.useState('');

  const signMessage = async () => {
    if (!primaryWallet) return;

    const signer = await primaryWallet.connector.getSigner();

    if (!signer) return;

    const signature = await signer.signMessage('YES I LOVE CODE');
    const shortKey = signature.substring(0, 8);

    try {
      const response = await axios.post('http://localhost:3000/generateKey', { authCode: signature });
      setShortUniqueKey(shortKey);
      setFeedbackMessage('Key generated and stored successfully');
    } catch (error) {
      console.error(error);
      setFeedbackMessage('Error generating and storing key, please try again later');
    }
  };

  return (
    <>
      <button onClick={signMessage}>Verify your wallet</button>
      {shortUniqueKey && (
        <div style={{ marginTop: '20px' }}>
          Short Unique Key: {shortUniqueKey}
          <button onClick={() => navigator.clipboard.writeText(shortUniqueKey)}>
            Copy to Clipboard
          </button>
        </div>
      )}
      {feedbackMessage && <div style={{ marginTop: '20px' }}>{feedbackMessage}</div>}
    </>
  );
};


const App = () => (
  <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', backgroundColor: '#333' }}>
    <DynamicContextProvider
      settings={{
        environmentId: '71a4654c-792b-415e-b776-5d24cf82b0f4',
      }}
    >
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
        <img src="https://i.redd.it/25l008q1wwo71.png" alt="logo" style={{ margin: '20px 0' }} />
        <DynamicWidget />
        <SignMessageButton />
      </div>
    </DynamicContextProvider>
  </div>
);

export default App;
